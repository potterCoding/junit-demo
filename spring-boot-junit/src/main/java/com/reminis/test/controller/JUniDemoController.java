package com.reminis.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JUniDemoController {

    @GetMapping("/hello")
    public String test() {
        return "hello, junit demo";
    }

}
