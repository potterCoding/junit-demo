package com.reminis.test.exception;

/**
 * @author sql
 * @version 1.0.0
 * @date 2021/9/11 11:00
 */
public class TicketException extends RuntimeException {
    public TicketException() {
    }

    public TicketException(String message) {
        super(message);
    }
}
