package com.reminis.test.entity;

import com.reminis.test.exception.TicketException;

/**
 * 车票测试
 */
public class TicketSeller {

    // 现有库存
    private Integer inventory;

    /**
     * 售票方法
     * @param num 售票数
     */
    public void sell(int num) {
        if (inventory < num) {
            throw new TicketException("all ticket sold out");
        }
        this.inventory = this.inventory - num;
    }

    /**
     * 退票方法
     * @param num 退票数
     */
    public void refund(int num) {
        this.inventory = this.inventory + num;
    }


    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }
}
